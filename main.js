const fill = document.querySelector('.fill');
const empties = document.querySelectorAll('.empty');

// * Fill Functions
const dragStart = event => {
  event.target.classList.add('hold');
  setTimeout(() => event.target.classList.add('invisible'), 0);
};

const dragEnd = event => {
  event.target.className = 'fill';
};

// * Empties functions
const dragOver = event => {
  event.preventDefault();

};

const dragEnter = event => {
  event.preventDefault();

  event.target.classList.add('hovered');
};

const dragLeave = event => {
  event.target.classList.remove('hovered');
};

const dragDrop = event => {
  event.target.append(fill);
  event.target.classList.remove('hovered');
};

// * Fill Listeners
fill.addEventListener('dragstart', dragStart);
fill.addEventListener('dragend', dragEnd);

// * Loop thru empties and call drag events
empties.forEach(empty => {
  empty.addEventListener('dragover', dragOver);
  empty.addEventListener('dragenter', dragEnter);
  empty.addEventListener('dragleave', dragLeave);
  empty.addEventListener('drop', dragDrop);
});
